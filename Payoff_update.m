%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = Payoff_update(D,V,C)
%D: 0- pusto, 1-Dove, 2-Hawk
%3wymiar : 1 tabela- jaki to ptak
%          2 tabela- fitness

for i=1:size(D,1)
   for j=1:size(D,2) 
       if D(i,j,1)==0 %jezeli jest pusto to przeskocz
           continue;
       end
       D(i,j,2)=0;  %fitness(i,j)=0
       %dla kazdego sasiada:
       sas = gdzieSasiad(D, i, j);
       for a=1:3
            for b=1:3
                if a==2 && b==2
                    continue;
                end
                if sas(a,b)==1                    
                    %payoff czyli to interaction(i,j,i-2+a,j-2+b) 
                    %update fitnessu f(i)=f(i)+payoff[i,j]
                    D(i,j,2)=D(i,j,2)+Payoff(D,i,j,i-2+a,j-2+b,V,C);
                end
            end
       end         
   end    
end
result=D;
   

