%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result =initialize (wym_row, wym_col, a)
if a == 1
        result=zeros(wym_row,wym_col,2);
        for i=1:wym_row
           for j=1:wym_col
                result(i,j,1)=fix(rand*3);
           end
        end
elseif a==2
    load Dplik
    result=D;    
else
    disp('Podano nieodpowiedni tryb pracy')
end

%save dane   % zapisuje caly workspace do pliku 'dane'
%save dane D   % zapisuje d do pliku - nadpisuje
%load dane   % wczytuje workspace z pliku 'dane'
