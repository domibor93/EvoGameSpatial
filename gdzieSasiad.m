%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sas = gdzieSasiad(D, i , j)
%macierz sas obrazuje gdzie sa sasiedzi
%jezeli jest sasiad na danej pozycji to 1, jezeli nie to 0,
sas=[1 1 1; 1 0 1; 1 1 1];

if i==1 %warunki brzegowe - czy w danym miejscu wgl moze byc sasiad
    sas(1,:)=0;
end
if j==1
    sas(:,1)=0;
end
if i==size(D,1)
    sas(3,:)=0;
end
if j==size(D,2)
    sas(:,3)=0;
end

for k=1:3 %czy sasiad w danym miejscu jest niezerowy
    for l=1:3
        if sas(k,l)~=0
            if D(i-2+k,j-2+l,1)==0
                sas(k,l)=0;
            end
        end
    end
end