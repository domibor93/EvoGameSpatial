%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = reproduction_probabilistic(D)

for i=1:size(D,1)
   for j=1:size(D,2)
       if D(i,j,1)==0 %jezeli jest "void"
           kaj=zeros(3,3,2);
           total_fit=0;
           %kaj - macierz sasiadow, 2 wymiar-fitness sasiadow:
           kaj(:,:,1) = gdzieSasiad(D,i,j);
           if max(max(kaj(:,:,1)))==0 %jezeli na okolo sa puste pola to idz dalej
               continue;
           end
            for k=1:3 %przypisywanie fitness
               for l=1:3
                   if kaj(k,l,1)~=0
                       kaj(k,l,2)=D(i-2+k, j-2+l ,2);
                   end
               end
            end
           
            for k=1:3 %liczenie total fitness
               for l=1:3
                   total_fit=total_fit+kaj(k,l,2);                   
               end
            end
            prob=zeros(3,3);
            for k=1:3 %liczenie  probability
               for l=1:3
                    prob(k,l)=kaj(k,l,2)/total_fit;
               end
            end
            suma_prob=0;
            for k=1:3 %robienie przedzia�ow
               for l=1:3
                   suma_prob=suma_prob+prob(k,l);
                 prob(k,l)=suma_prob; % wyznaczanie gornej granicy "przedzialow" dla prawdopodobie�twa
               end
            end
            los=rand; %losowa liczba to wyboru sasiada
            flag=1;            
            for k=1:3 %wyznaczenie sasiada
               for l=1:3
                    if los <= prob(k,l) && flag==1
                        D(i,j,:)=D(i-2+k,j-2+l,:);
                        flag=0;
                    end
               end
            end
            
       end
   end
end
result=D;