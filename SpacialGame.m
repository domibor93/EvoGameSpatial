%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%D: 0- pusto, 1-Dove, 2-Hawk
%3wymiar : 1 tabela- jaki to ptak D( , , 1)
%          2 tabela- fitness      D( , , 2)
clear;
rozmiar=20;
D=initialize(rozmiar,rozmiar, 2); %macierz kwadratowa 2-wczytaj z pliku 1-generuj randomow�

dov_init=0;
haw_init=0;
dov_end=0;
haw_end=0;
MAX=1000; % liczba iteracji
C=10;        %Value of resource
V=4;        %Cost of injury
morpro=0.3;     %mortality probability - a to na jakiej podstawie ? 

for i=1:size(D,1)
   for j=1:size(D,2)
       if D(i,j,1)==1
            dov_init=dov_init+1;
       elseif D(i,j,1) ==2
            haw_init=haw_init+1;
       end
   end
end

for i=1:MAX
    if i==10
    disp('blabla');
    end
   D=Payoff_update(D,V,C); %Done
   D=mortality_Semi_sychronous(D,morpro);
   %D=reproduction_deterministic(D);
   D=reproduction_probabilistic(D);
end


for i=1:size(D,1)
   for j=1:size(D,2)
       if D(i,j,1)==1
            dove_end=dove_end+1;
       elseif D(i,j,1) ==2
            haw_end=haw_end+1;
       end
   end
end

disp('Population at the beginning:');
disp(['Doves: ' num2str(dov_init) ' Hawks: ' num2str(haw_init) ]);

disp('Population at the end:');
disp(['Doves: ' num2str(dov_end) ' Hawks: ' num2str(haw_end) ]);