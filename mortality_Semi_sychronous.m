%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = mortality_Semi_sychronous(D,morpro)

%D: 0- pusto, 1-Dove, 2-Hawk
%3wymiar : 1 tabela- jaki to ptak
%          2 tabela- fitness


for i=1:size(D,1)
   for j=1:size(D,2) 
       if morpro > rand
            D(i,j,1)=0;
            D(i,j,2)=0;
       end
   end    
end
result=D;

