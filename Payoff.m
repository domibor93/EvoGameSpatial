%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = Payoff(D, i, j ,a, b, V, C)

%D: 0- pusto, 1-Dove, 2-Hawk
%3wymiar : 1 tabela- jaki to ptak D( , , 1)
%          2 tabela- fitness      D( , , 2)

if D(i,j,1)==1
     if D(a,b,1)==1 %dove/dove
         result=V/2;
     else   %dove/hawk
         result=0;
     end
elseif D(i,j,1)==2
    if D(a,b,1)==1 %hawk/dove
         result=V;
     else   %hawk/hawk
         result=(V-C)/2;
     end
else
    disp('UWAGA wywo�ano funkcj� Payoff dla pustego lub nieprawid�owego obiektu, program mo�e nie dzia�a� poprawnie');
    result=0;
end