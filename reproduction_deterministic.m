%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evolutionary Spatial Games          %
%           by Dominik Borkowski            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result=reproduction_deterministic(D)

       %D: 0- pusto, 1-Dove, 2-Hawk
%3wymiar : 1 tabela- jaki to ptak D( , , 1)
%          2 tabela- fitness      D( , , 2)

       
for i=1:size(D,1)
   for j=1:size(D,2)
       if D(i,j,1)==0 %jezeli jest "void"
           kaj=zeros(3,3,2);
           %kaj - macierz sasiadow, 2 wymiar-fitness sasiadow:
           kaj(:,:,1) = gdzieSasiad(D,i,j);
           if max(max(kaj(:,:,1)))==0 %jezeli na okolo sa puste pola to idz dalej
                  continue;
           end
           for k=1:3 %przypisywanie fitness
               for l=1:3
                   if kaj(k,l,1)~=0
                       kaj(k,l,2)=D(i-2+k, j-2+l ,2);
                   end
               end
           end
           maximum=max(max(kaj(:,:,2)));
           ilemax=0;
           for t1=1:3 %spradzanie czy jest wiecej niz jeden max
               for t2=1:3
                    if kaj(t1,t2,2)==maximum
                        ilemax=ilemax+1;
                    end
               end
           end
           if ilemax==1
               [tmp, a] = max(kaj(:,:,2));
               [~, b] = max(tmp, [], 2);
               a=a(b);
               D(i,j,:)=D(i-2+a,j-2+b,:);
           else
               maxy=zeros(ilemax,2);
               licz=1;
               for t1=1:3 %tworzenie wektroa maxow
                    for t2=1:3
                        if kaj(t1,t2,2)==maximum
                            maxy(licz,1)=t1;
                            maxy(licz,2)=t2;
                            licz=licz+1;
                        end
                    end
               end
               ktory=fix(1+rand*ilemax);               
              
               D(i,j,:)=D(i-2+maxy(ktory,1) , j-2+maxy(ktory,2) , : );
           end           
       end
   end
end
result=D;

%  disp(['maximum: ' num2str(maximum)]);
%                disp(['ilemax: ' num2str(ilemax)]);
%                disp(['i: ' num2str(i)  ' j: ' num2str(j)  ' ktory' num2str(ktory)]);
%                disp(['maxy(ktory,1): ' num2str(maxy(ktory,1)) ' maxy(ktory,2): ' num2str(maxy(ktory,1))]);
%                disp(['i-2+maxy(ktory,1): ' num2str(i-2+maxy(ktory,1)) ' j-2+maxy(ktory,2): ' num2str(j-2+maxy(ktory,1))]);
%                 disp(maxy);
%                 
%            disp('kaj(:,:,2):');
%                 disp(kaj(:,:,2));
%                 disp('maxy');
%                 disp(maxy);
